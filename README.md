# Code review SonarQube - или анализатор качества кода

## Подготовка окружения

### Внимание!

Роскомнадзор заблокировал сайты SonarQube(привет Российскому правительству) поэтому все ресурсы доступны только через ProxyServer. Скачайте и установите дополнительный плагин для Хром что бы попасть на адреса требуемых ресурсов.

Плагин для Google Chrome <https://www.hotspotshield.com/ru/>

### Скачиваем и устанавливаем

Для того что бы запустить SonarQube необходимо:  

* скачать и распаковать SonarQube <https://www.sonarqube.org/downloads/>  
* скачать и распаковать SonarQube Scanner <https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner> есть версии для разного окружения  
* установить в вашу среду разработки(например PHPStorm) плагин SonarLint качается из репозитория плагинов или по ссылке <https://www.sonarlint.org/>  
* установить Oracle JRE и OpenJDK **8 версии** <http://www.oracle.com/technetwork/java/javase/downloads/2133155> <http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html>  
* установить любой из БД серверов, например MySQL <https://downloads.mysql.com/archives/installer/>  

Подробнее о требованиях читайте здесь <https://docs.sonarqube.org/display/SONARQUBE67/Requirements>
 
### Принцип работы

Подробнее всего принцип работы расписан здесь <https://docs.sonarqube.org/display/SONARQUBE67/Architecture+and+Integration>

Но из того, что я понял, своими словами.  

1) SonarQube является сайтом который отобрарает статистику по всем вашим ресурсам  
2) SonarQube Scanner непосредственно занимается анализом ваших проектов, может запускаться по расписанию или по требованию(например GitHub) и отправляет результаты сканирования проекта SonarQube  
3) SonarLint(как я себе представляю) аналог SonarQube Scanner, который выполняет часть анализа кода "на лету" непосредственно во время работы над проектом. Как он связан с SonarQube(и связан ли вообще) я пока так и не понял.   
 
### Настраиваем

#### Настройка SonarQube Server

Настройка SonarQube Server, как правило сводится к настройке базы данных. Я крайне рекомендую использовать **postgresql**, этот сервер гораздо стабильнее в работе и позволяет работать с большими проектами  

```
sonar.jdbc.url=jdbc:postgresql://localhost/sonar
sonar.jdbc.username=sonarqube
sonar.jdbc.password=mypassword
```

#### Настройка SonarQube Console

SonarQube можно стартовать, без дополнительных настроек. Стартуется достаточно урезанная версия, но для первичной оценки проекта этого достаточно.

SonarQube Console не требует никаких дополнительных настроек. Как стартовать смотрите далее.

#### Настройка SonarQube Scanner

Настройка сканера представлена двумя файлами, которые прикреплены в данном репозитории. 

* **sonar-project.properties** - Находится непосредственно в проекте, хранит тонкие настройки сканирования проекта
* **sonar-scanner.properties** - Находится в папке настроек SonarQube Scanner, хранит общие настройки для всех проектов вашего сервера аналитики

### Запускаем

#### Запуск SonarQube Server

SonarQube Server запускается командой(для mac)  
```
./sonar.sh start 
```

или аналогичной для вашей операционной системы которая лежит по адресу

```
/sonarqube/bin/[you os] 
```

#### Запуск SonarQube Console

SonarQube Console запускается командой(для mac) 

```
./sonar.sh console 
```

или аналогичной для вашей операционной системы которая лежит по адресу

```
/sonarqube/bin/[you os] 
```

#### Запуск SonarQube Scanner

Команда для сканирования вашего проекта запускается из корня проекта

```
~/path/to/you/sonar/scanner/bin/sonar-scanner
```

### Автор

Автор документации [Камышников Вадим](https://vk.com/artskif)